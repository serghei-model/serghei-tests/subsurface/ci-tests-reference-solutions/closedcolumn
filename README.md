## closed column reference solution

This repository hosts the accepted correct solution of the closed column test problem, as defined in the `src/tests/closedcolumn.cpp`, which also corresponds to the [closedColumn test case](https://gitlab.com/serghei-model/serghei-tests/subsurface/analytical/closedColumn)


